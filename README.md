#### 介绍 此模版已转移到vue-template
vite+vue3+Element-plus+typeScript 搭建了一个干净清新的管理端项目系统

#### 搭建前准备

1. `Vscode`: 前端人必备写码神器
2. `Chrome`：对开发者非常友好的浏览器
3. `pnpm`： 速度快、节省磁盘空间的软件包管理器-https://www.pnpm.cn/

#### Vue2 与 Vue3 的区别

生命周期钩子变化:

```js
Vue2 ~~~~~~~~~~~ vue3
beforeCreate  -> setup()
created       -> setup()
beforeMount   -> onBeforeMount
mounted       -> onMounted
beforeUpdate  -> onBeforeUpdate
updated       -> onUpdated
beforeDestroy -> onBeforeUnmount
destroyed     -> onUnmounted
activated     -> onActivated
deactivated   -> onDeactivated
```

详情可查：https://www.yuque.com/docs/share/7d9fa18f-281c-4ae0-8242-e01cc791ec11?# 《vue3 与 vue2 的显著区别》

#### 介绍 vite

> Vite：下一代前端开发与构建工具

- 💡 极速的开发服务器启动
- ⚡️ 轻量快速的热模块重载（HMR）
- 🛠️ 丰富的功能
- 📦 自带优化的构建
- 🔩 通用的插件接口
- 🔑 完全类型化的 API `Vite` （法语意为 “迅速”，发音 /vit/）是一种全新的前端构建工具，它极大地改善了前端开发体验。

它主要由两部分组成：

- 一个开发服务器，它基于 原生 `ES` 模块 提供了 丰富的内建功能，如速度快到惊人的 模块热更新（HMR）。

- 一套构建指令，它使用 `Rollup` 打包你的代码，并且它是预配置的，可以输出用于生产环境的优化过的静态资源。

- Vite 意在提供开箱即用的配置，同时它的 插件 API 和 JavaScript API 带来了高度的`可扩展性`，并有完整的类型支持。

#### 使用 vite 快速创建脚手架

https://www.yuque.com/docs/share/4c486e07-3608-414a-a143-30ceaada980c?# 《管理端项目模版》

#### 软件架构说明

```js
- src api ----- API 模块化
      assets ---- 静态资源文件夹
        config ----- 按需引入组件与公用方法
        icons  ----- svg图标组件
        img ---- 图片
        locales --- 语言包（todo）
        scss ---- 公用样式
      components ---- 公用业务组件
      router ---- 路由
        permissions.ts  ---- 路由权限控制
      store  ---- pinia Vue 的存储库，它允许您跨组件/页面共享状态
      utils ---- 公用方法
      views ---- 业务组件
      shims-vue.d.ts ---- vue组件引用申明
```

#### 安装教程

```js
1.  git clone https://gitee.com/kerry0955/vue3-ts-admin.git
2.  cd vue3-ts-admin
3.  pnpm install
4.  pnpm dev
```

#### 使用插件

1. screenfull 插件常用方法：

screenfull.isFullscreen -- 检测全屏状态

screenfull.isEnabled -- 测试浏览器是否支持全 screenfull

.request(ele) 全屏 .exit() 退出全屏 .toggle() 切换全屏 .on(event, function) ： event 为 ‘change’ | ‘error’ 注册事件
.off(event, function) ： 移除前面已经注册的事件 .element： 返回一个全屏的 dom 节点，如果没有就为 null .isFullscreen : 是
否是全屏状态 .isEnabled ： 判断是否支持全屏
