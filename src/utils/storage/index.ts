import { isObject, isArray } from "../type"
interface itemOptions {
  name: string
  value: any
  expires: number //按小时为单位
  startTime?: number //记录何时将值存入缓存，毫秒级
}
// 默认缓存期限为15天
// const DEFAULT_CACHE_TIME = 60 * 60 * 24 * 15;
// todo crypto-js 对name加密解密
export const createStorage = ({ storage = window.localStorage } = {}) => {
  class Storage {
    private storage = storage
    // options
    set(params: itemOptions) {
      const obj = {
        name: "",
        value: "",
        expires: 0, //hours
        startTime: new Date().getTime() //记录何时将值存入缓存，毫秒级
      }
      const options = {} as itemOptions
      Object.assign(options, obj, params)
      if (options.expires > 0) {
        const num = options.expires * 60 * 60 * 1000
        options.expires = Math.round(num) // 转换成毫秒
        this.setItem(options.name, JSON.stringify(options))
      } else {
        const val = options.value || ""
        if (isObject(val) || isArray(val)) {
          this.setItem(options.name, JSON.stringify(val))
        }
      }
    }
    get(name: string) {
      let item: any = this.getItem(name)
      if (!item) return false
      try {
        item = JSON.parse(item)
      } catch (error) {
        item = ""
      }
      if (item.startTime) {
        const date = new Date().getTime()
        if (date - item.startTime > item.expires) {
          this.remove(name)
          return false
        } else {
          //缓存未过期，返回值
          return item.value
        }
      } else {
        return item
      }
    }
    remove(name: string) {
      this.storage.removeItem(name)
    }
    clear(): void {
      this.storage.clear()
    }
    //utils
    setItem(name: string, value: string) {
      this.storage.setItem(name, value)
    }
    getItem(name: string) {
      return this.storage.getItem(name)
    }
  }
  return new Storage()
}
