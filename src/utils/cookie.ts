export const getCookie = (name: string) => {
  const reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
  const arr = document.cookie.match(reg)
  if (arr) {
    return window.unescape(arr[2])
  } else {
    return null
  }
}

export const setCookie = (
  name: string,
  value: string,
  hours: number,
  path: string,
  domain: string
) => {
  const cookie = []
  // set value
  cookie.push(name + '=' + escape(value))
  // set expires time
  if (hours) {
    const exp = new Date()
    exp.setTime(exp.getTime() + hours * 60 * 60 * 1000)
    cookie.push('expires=' + exp.toUTCString())
  }
  path = path || '/'
  // set path
  cookie.push('path=' + path)
  // set domain
  if (domain) {
    cookie.push('domain=' + domain)
  }
  document.cookie = cookie.join(';')
}
export const delCookie = (name: string, path = '/', domain = '.meishakeji.com') => {
  setCookie(name, '', -24, path, domain)
}
