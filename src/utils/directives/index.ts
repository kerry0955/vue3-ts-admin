import { ObjectDirective, App } from 'vue';
const arr: any[] = [123, 2];
const auth: ObjectDirective = {
  mounted(el: HTMLButtonElement, binding) {
    const val = binding.value;
    if (val === undefined) return;
    if (!arr.includes(val)) {
      el.remove();
    }
  }
};

/**
 * 注册全局自定义指令
 * @param app
 */
export function setupDirectives(app: App) {
  // 权限控制指令
  app.directive('auth', auth);
}
