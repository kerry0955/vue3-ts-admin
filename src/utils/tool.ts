import { ElMessage } from 'element-plus'

export const msg = {
  suc: (message: string, duration = 1500) => ElMessage({ type: 'success', message, duration }),
  err: (message: string, duration = 1500) => ElMessage({ type: 'error', message, duration }),
  warn: (message: string, duration = 1500) => ElMessage({ type: 'warning', message, duration })
}

//auxiliary function
function format(value: number) {
  return value >= 10 ? value : '0' + value
}
export const dateFilter = function (time: number, type: number) {
  /* eslint complexity: ["error", 20] */
  if (time < 1) {
    return '-'
  }
  const date = new Date(time * 1000)
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hours = date.getHours()
  const minutes = date.getMinutes()
  const second = date.getSeconds()
  let result = null
  switch (type) {
    case 0: // 01-05
      result = `${format(month)}-${format(day)}`
      break
    case 1: // 11:12
      result = `${format(hours)}-${format(minutes)}`
      break
    case 2: // 2015-01-05
      result = `${year}-${format(month)}-${format(day)}`
      break
    case 3: // 2015-01-05 11:12
      result = `${year}-${format(month)}-${format(day)}  ${format(hours)}:${format(minutes)}`
      break
    case 4: // 2015-01-05 11:12:06
      result = `${year}-${format(month)}-${format(day)}  ${format(hours)}:${format(
        minutes
      )}:${format(second)}`
      break
    case 5: // h小时m分钟
      result = `${format(hours)}小时${format(minutes)}分钟`
      break
    case 6: // 2017年08月29日
      result = `${year}年${format(month)}月${format(day)}日`
      break
    case 7: // YYYY-MM-DD or YYYY-MM-DD HH:mm
      result = `${year}-${format(month)}-${format(day)}`
      ;(hours > 0 || minutes > 0) && (result += `  ${format(hours)}:${format(minutes)}`)
      break
    case 8: // 2019-02
      result = `${year}-${format(month)}`
      break
    default:
      result = `${year}-${format(month)}-${format(day)}  ${format(hours)}:${format(minutes)}`
      break
  }
  return result
}
/**
 * 获取url参数
 */
export const getUrlParam = (name: string) => {
  const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  const r = window.location.search.substr(1).match(reg)
  if (r != null) {
    return window.unescape(r[2])
  }
  return null
}
