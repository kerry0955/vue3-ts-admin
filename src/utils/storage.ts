import { isObject, isArray } from './type'
interface itemOptions {
  name: string
  value: any
  expires: number //按小时为单位
  startTime?: number //记录何时将值存入缓存，毫秒级
}
export default class Storage {
  private type: number
  constructor(type?: number) {
    this.type = type || 1
  }
  // options
  set(params: itemOptions) {
    const obj = {
      name: '',
      value: '',
      expires: 0,
      startTime: new Date().getTime() //记录何时将值存入缓存，毫秒级
    }
    const options = {} as itemOptions
    Object.assign(options, obj, params)
    if (options.expires > 0) {
      const num = options.expires * 60 * 60 * 1000
      options.expires = Math.round(num) // 转换成毫秒
      this.setItem(options.name, JSON.stringify(options))
    } else {
      const val = options.value || ''
      if (isObject(val) || isArray(val)) {
        this.setItem(options.name, JSON.stringify(val))
      }
    }
  }
  get(name: string) {
    let item: any = this.getItem(name)
    if (!item) return false
    try {
      item = JSON.parse(item)
    } catch (error) {
      // eslint-disable-next-line no-self-assign
    }
    if (item.startTime) {
      const date = new Date().getTime()
      if (date - item.startTime > item.expires) {
        this.remove(name)
        return false
      } else {
        //缓存未过期，返回值
        return item.value
      }
    } else {
      return item
    }
  }
  remove(name: string) {
    this.type === 1 ? window.localStorage.removeItem(name) : window.sessionStorage.removeItem(name)
  }
  clear() {
    window.localStorage.clear()
    window.sessionStorage.clear()
  }
  //utils
  setItem(name: string, value: string) {
    this.type === 1
      ? window.localStorage.setItem(name, value)
      : window.sessionStorage.setItem(name, value)
  }
  getItem(name: string) {
    return this.type === 1 ? window.localStorage.getItem(name) : window.sessionStorage.getItem(name)
  }
}
