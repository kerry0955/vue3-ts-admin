import App from './App.vue';
import { router, setupRouter } from './router/index';
import { setupStore } from './store/index';
import { setupDirectives } from './utils/directives';

import { setupSvgIcon } from './assets/icons/index';

import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import 'element-plus/theme-chalk/el-message.css';

async function bootstrap() {
  const app = createApp(App);
  //store
  setupStore(app);
  // 注册全局自定义指令如： v-permission 权限指令
  setupDirectives(app);
  setupSvgIcon(app);

  // registry icons
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component);
  }
  // router
  await setupRouter(app);
  await router.isReady();
  app.mount('#app'); //mounted
}
void bootstrap();
