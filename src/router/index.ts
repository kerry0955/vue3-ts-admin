import type { App } from 'vue';
import { createRouter, createWebHistory, createWebHashHistory, RouteRecordRaw, Router } from 'vue-router';
import HomePage from '@/components/layout/index.vue';
import { filterRoutes } from './permissions';
const files = import.meta.globEager('./modules/*.ts');
const routes: RouteRecordRaw[] = [
  {
    // path: '/:pathMatch(.*)*',
    hide: true,
    path: '/:path(.*)*',
    name: 'ErrorPage',
    component: () => import('@/components/404.vue')
  },
  // must RootRoute redirect
  {
    path: '/',
    name: 'HomePage',
    component: HomePage,
    redirect: '',
    children: [
      {
        path: '',
        name: 'Welcome',
        component: () => import('@/views/welcome.vue')
      }
    ]
  }
];
const createRouterGuards = function (router: Router) {
  const whiteNameList = ['Login'];
  let asyncRoute: RouteRecordRaw[] = [];
  router.beforeEach(async (to, from, next) => {
    // Whitelist can be directly entered
    if (whiteNameList.includes(to.name)) {
      next();
      return;
    }
    if (asyncRoute.length === 0) {
      for (const key in files) {
        files[key].default && asyncRoute.push(...files[key].default);
      }
      //filter asyncRoute
      await filterRoutes(asyncRoute);
      // 动态添加可访问路由表
      asyncRoute.forEach((item) => {
        router.addRoute(item as unknown as RouteRecordRaw);
      });
      // next({ ...to, replace: true });
      next({ path: to.path });
    } else {
      next();
    }

    // router.addRoute(asyncRoute);
    console.log('router', router.getRoutes(), router.options.routes);

    // next();
  });
  router.afterEach(() => {});
  router.onError(() => {});
};

export const router = createRouter({
  history: createWebHistory(),
  // history: createWebHashHistory(''),
  routes,
  strict: true,
  scrollBehavior: () => ({ left: 0, top: 0 })
});

// use router
export function setupRouter(app: App<Element>) {
  app.use(router);
  // 创建路由守卫
  createRouterGuards(router);
}
