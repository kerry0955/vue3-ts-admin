import HomePage from '@/components/layout/index.vue';
import { RouteRecordRaw } from 'vue-router';
const routes: Array<RouteRecordRaw> = [
  {
    path: '/table',
    name: 'tablePage',
    component: HomePage,
    redirect: '/table/file',
    children: [
      {
        path: 'file',
        name: 'tablefilePage',
        component: () => import('@/views/table.vue'),
        meta: {
          icon: 'AppstoreFilled',
          title: '文档管理',
          auth: '22d724743fb2e' // 路由权限标识
        }
      }
    ]
  }
];

export default routes;
