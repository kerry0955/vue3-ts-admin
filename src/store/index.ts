import type { App } from 'vue'

// register store
const pinia = createPinia()
export function setupStore(app: App<Element>) {
  app.use(pinia)
}

// export * from './modules/app'

// expose all modules

// const modules = {} as any
// const files = import.meta.globEager('./modules/*.ts')
// for (const key in files) {
//   modules[key.replace(/(\.\/modules\/|\.ts)/g, '')] = files[key].default
// }

// export default modules
