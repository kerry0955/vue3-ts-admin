import { getConfig, setConfig } from '../../assets/locales/config';
// import { changevantLocale } from '@/config/locales/index'
const config = getConfig();
type AppStateType = {
  loading?: boolean;
  theme?: string;
  locale?: string;
};
export const useAppStore = defineStore('t-app', {
  state: (): AppStateType => ({
    loading: false,
    // theme: config.theme as string,
    locale: config.locale as string
  }),
  actions: {
    updateSettings(partial: Partial<AppStateType>) {
      this.$patch(partial);
    }
    // changeTheme(theme?: string) {
    //   if (theme) {
    //     this.theme = theme
    //   } else {
    //     this.theme = this.theme === 'light' ? 'dark' : 'light'
    //   }
    //   setConfig({ theme })
    // },
    // changeLocale(locale?: 'zh' | 'en') {
    //   const key = changevantLocale(locale)
    //   this.locale = key
    //   setConfig({ locale: key })
    // }
  }
});
