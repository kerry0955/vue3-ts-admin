import type { App } from "vue"
import svgIcon from "./svgIcon.vue"
import "virtual:svg-icons-register"

// use router
export function setupSvgIcon(app: App<Element>) {
  app.component("SvgIcon", svgIcon)
}
