import zhCN from './zh-CN';
import enUS from './en-US';
import { createI18n } from 'vue-i18n';
import { App } from 'vue';

// import { Locale } from 'vant'
// import enLocale from 'vant/es/locale/lang/en-US'
// import zhLocale from 'vant/es/locale/lang/zh-CN'
import { getConfig } from './config';

const i18n = createI18n({
  legacy: false,
  locale: getConfig().locale, //en
  fallbackLocale: 'en',
  messages: {
    zh: zhCN,
    en: enUS
  }
});

export function useI18n(app: App) {
  app.use(i18n);
}

export const t = (key: string) => i18n.global.t(key);
/**
 * 设置语言
 * @param key string | undefined
 */
export function changevantLocale(key?: 'zh' | 'en') {
  if (!key) {
    key = i18n.global.locale.value === 'zh' ? 'en' : 'zh';
  }
  i18n.global.locale.value = key;
  key === 'zh' ? Locale.use('zh-CN', zhLocale) : Locale.use('en-US', enLocale);
  return key;
}
