export default {
  username: '用户名',
  password: '密码',
  forgotPassword: '忘记密码/解绑',
  login: '登录',
  register: '注册',
  success: '成功',
  pleaseInput: '请输入',
  home: {
    title: '首页',
  },
  category: {
    title: '分类'
  },
  user: {
    title: '我的',
    logout: '退出登录'
  }
}
