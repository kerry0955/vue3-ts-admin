export default {
  username: 'Username',
  password: 'Password',
  forgotPassword: 'forget the password',
  login: 'Login',
  register: 'Register',
  success: 'Success',
  pleaseInput: 'Please Enter',
  home: {
    title: 'Home',
  },
  category: {
    title: 'Category'
  },
  user: {
    title: 'User',
    logout: 'Logout'
  }
}
