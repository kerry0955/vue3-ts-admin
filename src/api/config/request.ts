import axios from 'axios';
import type { AxiosInstance, AxiosResponse } from 'axios';

import { BASE_URL } from '../ENV.js';
import { AxiosCanceler } from './axiosCancel';
import { ElMessage } from 'element-plus';
import { useAppStore } from '@/store/modules/app';

interface CreateAxiosOptions {
  repeat?: boolean; // 是否开启取消重复请求, 默认为 true
  statusErrShow?: boolean; // 是否开启接口错误信息展示,默认为true
  codeErrShow?: boolean; // 是否开启code不为0时的信息提示, 默认为false
  loading?: boolean; // 是否开启loading，默认false
  cache?: string; // 是否缓存（pinia）
  storage?: string; // 开启缓存 sessionStorage 权重小于cache
}
// utils
/**
 * 处理异常
 * @param {*} error
 */
function httpErrorStatusHandle(error: any): void {
  // 处理被取消的请求
  if (axios.isCancel(error)) return console.error('重复请求：' + error.message);
  let message = '';
  if (error && error.response) {
    switch (error.response.status) {
      case 302:
        message = '接口重定向了！';
        break;
      case 400:
        message = '参数不正确！';
        break;
      case 401:
        message = '您未登录，或者登录已经超时，请先登录！';
        break;
      case 403:
        message = '您没有权限操作！';
        break;
      case 404:
        message = `请求地址出错: ${error.response.config.url}`;
        break; // 在正确域名下
      case 408:
        message = '请求超时！';
        break;
      case 409:
        message = '系统已存在相同数据！';
        break;
      case 500:
        message = '服务器内部错误！';
        break;
      case 501:
        message = '服务未实现！';
        break;
      case 502:
        message = '网关错误！';
        break;
      case 503:
        message = '服务不可用！';
        break;
      case 504:
        message = '服务暂时无法访问，请稍后再试！';
        break;
      case 505:
        message = 'HTTP版本不受支持！';
        break;
      default:
        message = '异常问题，请联系管理员！';
        break;
    }
  }
  if (error.message.includes('timeout')) {
    message = '网络请求超时！';
  }
  if (error.message.includes('Network')) {
    message = window.navigator.onLine ? '服务端异常！' : '您断网了！';
  }
  // if (error.code === "ERR_CANCELED") {
  //   message = "请勿重复操作！"
  // }
  // Toast(message);
}
class MyAxios {
  private axiosInstance: AxiosInstance;
  private options: CreateAxiosOptions;
  private appStore: any;

  constructor(opt: CreateAxiosOptions) {
    this.options = opt;
    this.axiosInstance = axios.create({
      baseURL: BASE_URL,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
        // 'Content-Type': 'multipart/form-data',
        // 'application/json' ||'multipart/form-data'
      },
      transformRequest: [
        function (data, headers: any) {
          if (headers['Content-Type'] === 'application/x-www-form-urlencoded') {
            const params = new window.URLSearchParams();
            params.append('data', JSON.stringify(data));
            return params;
          } else {
            return data;
          }
        }
      ],
      withCredentials: false,
      timeout: 15000 // 超时
    });
    this.appStore = useAppStore();
    this.setupInterceptors();
  }
  getAxios(): AxiosInstance {
    return this.axiosInstance;
  }
  setAuth(config: any) {
    const href = window.location.href;

    return config;
  }
  /**
   * @description: 拦截器配置
   */
  private setupInterceptors() {
    const opt = this.options;
    const axiosCanceler = new AxiosCanceler();

    this.axiosInstance.interceptors.request.use(
      (config: any) => {
        opt.repeat && axiosCanceler.addPending(config);
        opt.loading && this.appStore.updateSettings({ loading: true });
        return this.setAuth(config);
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    // 响应结果拦截器处理
    this.axiosInstance.interceptors.response.use(
      (res: AxiosResponse<any>) => {
        opt.repeat && axiosCanceler.removePending(res.config);
        opt.loading && this.appStore.updateSettings({ loading: false });
        // code不为0时
        if (opt.codeErrShow && res.data && res.data.code !== 0) {
          ElMessage(res.data.msg || res.data.message);
          return Promise.reject(res.data); // code不等于0, 页面具体逻辑就不执行了
        } else {
          return res.data;
        }
      },
      (error) => {
        error.config && axiosCanceler.removePending(error.config);
        opt.statusErrShow && httpErrorStatusHandle(error); // 处理错误状态码
        opt.loading && this.appStore.updateSettings({ loading: false });

        return Promise.reject(error);
      }
    );
  }
}
export const $http = function (opt: CreateAxiosOptions) {
  return new MyAxios(
    Object.assign(
      {
        loading: false,
        repeat: true,
        statusErrShow: true,
        codeErrShow: false,
        cache: '',
        storage: ''
      },
      opt
    )
  ).getAxios();
};
