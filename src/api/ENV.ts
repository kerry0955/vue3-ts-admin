const ORIGIN = window.location.origin
// 判断是否为开发环境
const IS_DEV = /localhost|192.168/i.test(ORIGIN)
// 判断是否为测试环境
const IS_TEST = /test/i.test(ORIGIN)

// 判断是否为预发布环境
const IS_PRE = /pre/i.test(ORIGIN)

const API_BASE_URL = ''
// if (IS_DEV) {
// } else if (IS_TEST) {
// }
export const ENV = {
  IS_DEV,
  IS_TEST,
  IS_PRE
}
export const BASE_URL = API_BASE_URL
