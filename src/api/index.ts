// Generated
const modules: any = {}

const files = import.meta.globEager('./modules/*.ts')
for (const key in files) {
  modules[key.replace(/(\.\/modules\/|\.js)/g, '')] = files[key].default
}

export default modules
