import { resolve } from 'path';
import type { UserConfig, ConfigEnv } from 'vite';
import vue from '@vitejs/plugin-vue';

import AutoImport from 'unplugin-auto-import/vite';
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons';

import vueJsx from '@vitejs/plugin-vue-jsx';
import Components from 'unplugin-vue-components/vite';
// https://element-plus.org/zh-CN/#/zh-CN
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

// open gzip minimize
import viteCompression from 'vite-plugin-compression';
// import vueI18n from '@intlify/vite-plugin-vue-i18n'
const pathResolve = (dir: string): string => {
  return resolve(__dirname, '.', dir);
};
const root = process.cwd();

const createPlugin = [
  vue(),
  vueJsx(),
  AutoImport({
    imports: ['vue', 'vue-router', 'pinia'], // import Specify module
    dts: resolve(__dirname, './src/assets/config/auto-import.d.ts'), // appoint some function default:true(all fn)
    include: [
      /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
      /\.vue$/
    ]
  }),
  Components({
    dts: resolve(__dirname, './src/assets/config/components.d.ts'),
    resolvers: [ElementPlusResolver()]
  }),
  createSvgIconsPlugin({
    // 指定需要缓存的图标文件夹
    iconDirs: [resolve(process.cwd(), 'src/assets/icons/svg')],
    // 指定symbolId格式
    symbolId: 'icon-[dir]-[name]'

    /**
     * 自定义插入位置
     * @default: body-last
     */
    // inject?: 'body-last' | 'body-first'

    /**
     * custom dom id
     * @default: __svg__icons__dom__
     */
    // customDomId: '__svg__icons__dom__',
  })
];
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfig => {
  // console.log('command, mode:::', command, mode)//serve development ||  build production
  const isBuild = command === 'build';

  if (isBuild) {
    createPlugin.push(
      viteCompression({
        verbose: true,
        disable: false,
        threshold: 10240,
        algorithm: 'gzip',
        ext: '.gz'
      })
    ); //https://github.com/vbenjs/vite-plugin-compression/blob/main/README.zh_CN.md
  }
  return {
    root,
    plugins: createPlugin,
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/scss/variable.scss";'
        }
      }
    },
    resolve: {
      alias: {
        '@': pathResolve('src'),
        '@api': pathResolve('src/api/modules'),
        '@s': pathResolve('src/store/modules'),
        '@img': pathResolve('src/assets/img')
      }
    },
    server: {
      hmr: { overlay: false }, //设置 server.hmr.overlay 为 false 可以禁用开发服务器错误的屏蔽。
      open: true,
      cors: true,
      host: '0.0.0.0',
      proxy: {
        '/api': {
          target: '***',
          changeOrigin: true
        }
      }
    },
    build: {
      outDir: 'dist',
      assetsDir: 'assets',
      sourcemap: false,
      minify: 'esbuild',
      cssCodeSplit: true,
      // Turning off brotliSize display can slightly reduce packaging time
      chunkSizeWarningLimit: 1500
    }
  };
};
